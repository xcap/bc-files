#!/bin/bash

merge_nescheck_bc_files() {
        echo "llvm-link -o $1 $2 $3"
        llvm-link-10 -o $1 $2 $3
}

merge_bc_files() {
	echo "llvm-link -o $1 $2 $3"
	llvm-link-10 -o $1 $2 $3
}

DRIVER=$1
PDG="$(pwd)/../pdg/build/libpdg.so"
LIBLCD_FUNCS="$(pwd)/liblcd_funcs.txt"
# KERNEL_BC="/local/device/linux-llvm/vmlinux.bc"

driver_name=${DRIVER}
ko_bc=${driver_name}.ko.bc
kernel_bc=${driver_name}_kernel.bc
merged_bc=${driver_name}.bc

#merging
if [ ! -s ${merged_bc} ]; then
  echo -e "\e[32m Merging BC files \e[0m"
  merge_bc_files ${merged_bc} ${ko_bc} ${kernel_bc}
else
  echo -e "\e[32m [Note]: found merged bc file \e[0m"
fi
echo -e "\e[32m Output boundary info ${ko_bc} \e[0m"
opt-10 -load ${PDG} -libfile ${LIBLCD_FUNCS} -output-boundary-info < ${ko_bc}
echo -e "\e[32m Computing shared data on ${merged_bc} \e[0m"
if [ ! -s shared_struct_types ]; then
  echo -e "\e[33m [Warning]: generating shared struct types \e[0m"
  opt-10 -load ${PDG} -shared-data < ${merged_bc}
else
  echo -e "\e[32m [Note]: found shared struct types \e[0m"
fi
echo -e "\e[32m Output boundary info ${ko_bc} \e[0m"

# output boundary
opt-10 -load ${PDG} -libfile ${LIBLCD_FUNCS} -output-boundary-info < ${ko_bc}

# run nescheck analysis
echo -e "\e[32m Running data access analysis on ${merged_bc} \e[0m"
opt-10 -load ${PDG} -daa -analysis-stats=true -time-passes < ${merged_bc} 2> daa_stats &

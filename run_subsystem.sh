#!/bin/bash

BC_FILES_DIR="$(pwd)"
SCRIPT_DIR="$(dirname "$BC_FILES_DIR")"

for d in */ ; do
  pushd ${d} > /dev/null
  bench_name="${PWD##*/}"
  if [ ! -f "table2" ]; then
    echo "processing  ${bench_name}"
    source "${SCRIPT_DIR}/run_nescheck.sh" ${bench_name}
  fi
  popd > /dev/null
done

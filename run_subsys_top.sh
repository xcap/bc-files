#!/bin/bash

for subsys in block edac hwmon usb; do
  pushd ${subsys};
  sudo ../run_subsystem.sh &
  popd;
done


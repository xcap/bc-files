#!/bin/bash

BASE_DIR="$(pwd)"
SUBSYS_LIST="${BASE_DIR}/subsystem_list"
STATS_DIR="${BASE_DIR}/benchmark_stats"

mkdir -p ${STATS_DIR}/table2

while IFS= read -r line
do
	SUBSYS_DIR="${BASE_DIR}/${line}"
	pushd ${SUBSYS_DIR} > /dev/null
	subsys_name="${PWD##*/}"
	echo "copying ${subsys_name} stats"
	sudo cp table2_stats ${STATS_DIR}/table2/${subsys_name}.csv
	popd > /dev/null
done < "${SUBSYS_LIST}"

#!/bin/bash

BASE_DIR="$(pwd)"
DRIVER_LIST="${BASE_DIR}/driver_list"
STATS_DIR="${BASE_DIR}/benchmark_stats"
while IFS= read -r line
do
	DRIVER_IDL_DIR="${BASE_DIR}/${line}"
	pushd ${DRIVER_IDL_DIR} > /dev/null
	bench_name="${PWD##*/}"
	echo "copying ${bench_name} stats"
	sudo cp table1 ${STATS_DIR}/${bench_name}.csv
	popd > /dev/null
done < "${DRIVER_LIST}"

#!/bin/bash

pass_name=$1
bc_file_name=$2

PDG="/local/device/program-dependence-graph/build/libpdg.so"
LIBLCD_FUNCS="/local/device/bc-files/liblcd_funcs.txt"

opt-10 -load ${PDG} -${pass_name} -time-passes -libfile ${LIBLCD_FUNCS} < ${bc_file_name}.bc 2> err.log
